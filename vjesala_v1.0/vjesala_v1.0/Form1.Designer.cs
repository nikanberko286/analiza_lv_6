﻿namespace GuessTheWord
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_A = new System.Windows.Forms.Button();
            this.button_B = new System.Windows.Forms.Button();
            this.button_C = new System.Windows.Forms.Button();
            this.button_D = new System.Windows.Forms.Button();
            this.button_E = new System.Windows.Forms.Button();
            this.button_F = new System.Windows.Forms.Button();
            this.button_G = new System.Windows.Forms.Button();
            this.button_H = new System.Windows.Forms.Button();
            this.button_M = new System.Windows.Forms.Button();
            this.button_N = new System.Windows.Forms.Button();
            this.button_O = new System.Windows.Forms.Button();
            this.button_P = new System.Windows.Forms.Button();
            this.button_K = new System.Windows.Forms.Button();
            this.button_L = new System.Windows.Forms.Button();
            this.button_J = new System.Windows.Forms.Button();
            this.button_I = new System.Windows.Forms.Button();
            this.button_U = new System.Windows.Forms.Button();
            this.button_V = new System.Windows.Forms.Button();
            this.button_S = new System.Windows.Forms.Button();
            this.button_T = new System.Windows.Forms.Button();
            this.button_R = new System.Windows.Forms.Button();
            this.button_Z = new System.Windows.Forms.Button();
            this.label_MissedLetters = new System.Windows.Forms.Label();
            this.button_LoadNewWord = new System.Windows.Forms.Button();
            this.label_MissedLtrCnt = new System.Windows.Forms.Label();
            this.label_Word = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_A
            // 
            this.button_A.Location = new System.Drawing.Point(27, 137);
            this.button_A.Name = "button_A";
            this.button_A.Size = new System.Drawing.Size(38, 23);
            this.button_A.TabIndex = 0;
            this.button_A.Text = "A";
            this.button_A.UseVisualStyleBackColor = true;
            this.button_A.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_B
            // 
            this.button_B.Location = new System.Drawing.Point(71, 137);
            this.button_B.Name = "button_B";
            this.button_B.Size = new System.Drawing.Size(38, 23);
            this.button_B.TabIndex = 1;
            this.button_B.Text = "B";
            this.button_B.UseVisualStyleBackColor = true;
            this.button_B.Click += new System.EventHandler(this.button_B_Click);
            // 
            // button_C
            // 
            this.button_C.Location = new System.Drawing.Point(115, 137);
            this.button_C.Name = "button_C";
            this.button_C.Size = new System.Drawing.Size(38, 23);
            this.button_C.TabIndex = 3;
            this.button_C.Text = "C";
            this.button_C.UseVisualStyleBackColor = true;
            this.button_C.Click += new System.EventHandler(this.button_C_Click);
            // 
            // button_D
            // 
            this.button_D.Location = new System.Drawing.Point(159, 137);
            this.button_D.Name = "button_D";
            this.button_D.Size = new System.Drawing.Size(38, 23);
            this.button_D.TabIndex = 2;
            this.button_D.Text = "D";
            this.button_D.UseVisualStyleBackColor = true;
            this.button_D.Click += new System.EventHandler(this.button_D_Click);
            // 
            // button_E
            // 
            this.button_E.Location = new System.Drawing.Point(203, 137);
            this.button_E.Name = "button_E";
            this.button_E.Size = new System.Drawing.Size(38, 23);
            this.button_E.TabIndex = 7;
            this.button_E.Text = "E";
            this.button_E.UseVisualStyleBackColor = true;
            this.button_E.Click += new System.EventHandler(this.button_E_Click);
            // 
            // button_F
            // 
            this.button_F.Location = new System.Drawing.Point(247, 137);
            this.button_F.Name = "button_F";
            this.button_F.Size = new System.Drawing.Size(38, 23);
            this.button_F.TabIndex = 6;
            this.button_F.Text = "F";
            this.button_F.UseVisualStyleBackColor = true;
            this.button_F.Click += new System.EventHandler(this.button_F_Click);
            // 
            // button_G
            // 
            this.button_G.Location = new System.Drawing.Point(291, 137);
            this.button_G.Name = "button_G";
            this.button_G.Size = new System.Drawing.Size(38, 23);
            this.button_G.TabIndex = 5;
            this.button_G.Text = "G";
            this.button_G.UseVisualStyleBackColor = true;
            this.button_G.Click += new System.EventHandler(this.button_G_Click);
            // 
            // button_H
            // 
            this.button_H.Location = new System.Drawing.Point(335, 137);
            this.button_H.Name = "button_H";
            this.button_H.Size = new System.Drawing.Size(38, 23);
            this.button_H.TabIndex = 4;
            this.button_H.Text = "H";
            this.button_H.UseVisualStyleBackColor = true;
            this.button_H.Click += new System.EventHandler(this.button_H_Click);
            // 
            // button_M
            // 
            this.button_M.Location = new System.Drawing.Point(203, 166);
            this.button_M.Name = "button_M";
            this.button_M.Size = new System.Drawing.Size(38, 23);
            this.button_M.TabIndex = 15;
            this.button_M.Text = "M";
            this.button_M.UseVisualStyleBackColor = true;
            this.button_M.Click += new System.EventHandler(this.button_M_Click);
            // 
            // button_N
            // 
            this.button_N.Location = new System.Drawing.Point(247, 166);
            this.button_N.Name = "button_N";
            this.button_N.Size = new System.Drawing.Size(38, 23);
            this.button_N.TabIndex = 14;
            this.button_N.Text = "N";
            this.button_N.UseVisualStyleBackColor = true;
            this.button_N.Click += new System.EventHandler(this.button_N_Click);
            // 
            // button_O
            // 
            this.button_O.Location = new System.Drawing.Point(291, 166);
            this.button_O.Name = "button_O";
            this.button_O.Size = new System.Drawing.Size(38, 23);
            this.button_O.TabIndex = 13;
            this.button_O.Text = "O";
            this.button_O.UseVisualStyleBackColor = true;
            this.button_O.Click += new System.EventHandler(this.button_O_Click);
            // 
            // button_P
            // 
            this.button_P.Location = new System.Drawing.Point(335, 166);
            this.button_P.Name = "button_P";
            this.button_P.Size = new System.Drawing.Size(38, 23);
            this.button_P.TabIndex = 12;
            this.button_P.Text = "P";
            this.button_P.UseVisualStyleBackColor = true;
            this.button_P.Click += new System.EventHandler(this.button_P_Click);
            // 
            // button_K
            // 
            this.button_K.Location = new System.Drawing.Point(115, 166);
            this.button_K.Name = "button_K";
            this.button_K.Size = new System.Drawing.Size(38, 23);
            this.button_K.TabIndex = 11;
            this.button_K.Text = "K";
            this.button_K.UseVisualStyleBackColor = true;
            this.button_K.Click += new System.EventHandler(this.button_K_Click);
            // 
            // button_L
            // 
            this.button_L.Location = new System.Drawing.Point(159, 166);
            this.button_L.Name = "button_L";
            this.button_L.Size = new System.Drawing.Size(38, 23);
            this.button_L.TabIndex = 10;
            this.button_L.Text = "L";
            this.button_L.UseVisualStyleBackColor = true;
            this.button_L.Click += new System.EventHandler(this.button_L_Click);
            // 
            // button_J
            // 
            this.button_J.Location = new System.Drawing.Point(71, 166);
            this.button_J.Name = "button_J";
            this.button_J.Size = new System.Drawing.Size(38, 23);
            this.button_J.TabIndex = 9;
            this.button_J.Text = "J";
            this.button_J.UseVisualStyleBackColor = true;
            this.button_J.Click += new System.EventHandler(this.button_J_Click);
            // 
            // button_I
            // 
            this.button_I.Location = new System.Drawing.Point(27, 166);
            this.button_I.Name = "button_I";
            this.button_I.Size = new System.Drawing.Size(38, 23);
            this.button_I.TabIndex = 8;
            this.button_I.Text = "I";
            this.button_I.UseVisualStyleBackColor = true;
            this.button_I.Click += new System.EventHandler(this.button_I_Click);
            // 
            // button_U
            // 
            this.button_U.Location = new System.Drawing.Point(159, 195);
            this.button_U.Name = "button_U";
            this.button_U.Size = new System.Drawing.Size(38, 23);
            this.button_U.TabIndex = 23;
            this.button_U.Text = "U";
            this.button_U.UseVisualStyleBackColor = true;
            this.button_U.Click += new System.EventHandler(this.button_U_Click);
            // 
            // button_V
            // 
            this.button_V.Location = new System.Drawing.Point(203, 195);
            this.button_V.Name = "button_V";
            this.button_V.Size = new System.Drawing.Size(38, 23);
            this.button_V.TabIndex = 22;
            this.button_V.Text = "V";
            this.button_V.UseVisualStyleBackColor = true;
            this.button_V.Click += new System.EventHandler(this.button_V_Click);
            // 
            // button_S
            // 
            this.button_S.Location = new System.Drawing.Point(71, 195);
            this.button_S.Name = "button_S";
            this.button_S.Size = new System.Drawing.Size(38, 23);
            this.button_S.TabIndex = 19;
            this.button_S.Text = "S";
            this.button_S.UseVisualStyleBackColor = true;
            this.button_S.Click += new System.EventHandler(this.button_S_Click);
            // 
            // button_T
            // 
            this.button_T.Location = new System.Drawing.Point(115, 195);
            this.button_T.Name = "button_T";
            this.button_T.Size = new System.Drawing.Size(38, 23);
            this.button_T.TabIndex = 18;
            this.button_T.Text = "T";
            this.button_T.UseVisualStyleBackColor = true;
            this.button_T.Click += new System.EventHandler(this.button_T_Click);
            // 
            // button_R
            // 
            this.button_R.Location = new System.Drawing.Point(27, 195);
            this.button_R.Name = "button_R";
            this.button_R.Size = new System.Drawing.Size(38, 23);
            this.button_R.TabIndex = 17;
            this.button_R.Text = "R";
            this.button_R.UseVisualStyleBackColor = true;
            this.button_R.Click += new System.EventHandler(this.button_R_Click);
            // 
            // button_Z
            // 
            this.button_Z.Location = new System.Drawing.Point(247, 195);
            this.button_Z.Name = "button_Z";
            this.button_Z.Size = new System.Drawing.Size(38, 23);
            this.button_Z.TabIndex = 25;
            this.button_Z.Text = "Z";
            this.button_Z.UseVisualStyleBackColor = true;
            this.button_Z.Click += new System.EventHandler(this.button_Z_Click);
            // 
            // label_MissedLetters
            // 
            this.label_MissedLetters.AutoSize = true;
            this.label_MissedLetters.Font = new System.Drawing.Font("Microsoft YaHei UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_MissedLetters.ForeColor = System.Drawing.Color.Black;
            this.label_MissedLetters.Location = new System.Drawing.Point(122, 98);
            this.label_MissedLetters.Name = "label_MissedLetters";
            this.label_MissedLetters.Size = new System.Drawing.Size(75, 26);
            this.label_MissedLetters.TabIndex = 27;
            this.label_MissedLetters.Text = "label1";
            // 
            // button_LoadNewWord
            // 
            this.button_LoadNewWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_LoadNewWord.Location = new System.Drawing.Point(291, 195);
            this.button_LoadNewWord.Name = "button_LoadNewWord";
            this.button_LoadNewWord.Size = new System.Drawing.Size(82, 23);
            this.button_LoadNewWord.TabIndex = 29;
            this.button_LoadNewWord.Text = "Restart";
            this.button_LoadNewWord.UseVisualStyleBackColor = true;
            this.button_LoadNewWord.Click += new System.EventHandler(this.button_LoadNewWord_Click);
            // 
            // label_MissedLtrCnt
            // 
            this.label_MissedLtrCnt.AutoSize = true;
            this.label_MissedLtrCnt.BackColor = System.Drawing.Color.Transparent;
            this.label_MissedLtrCnt.Font = new System.Drawing.Font("Arimo", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_MissedLtrCnt.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label_MissedLtrCnt.Location = new System.Drawing.Point(269, 63);
            this.label_MissedLtrCnt.Name = "label_MissedLtrCnt";
            this.label_MissedLtrCnt.Size = new System.Drawing.Size(22, 24);
            this.label_MissedLtrCnt.TabIndex = 0;
            this.label_MissedLtrCnt.Text = "5";
            // 
            // label_Word
            // 
            this.label_Word.AutoSize = true;
            this.label_Word.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_Word.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_Word.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Word.Location = new System.Drawing.Point(178, 21);
            this.label_Word.Name = "label_Word";
            this.label_Word.Size = new System.Drawing.Size(151, 31);
            this.label_Word.TabIndex = 0;
            this.label_Word.Text = "________";
            this.label_Word.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(23, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pogodi rijec";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(22, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 26);
            this.label1.TabIndex = 28;
            this.label1.Text = "Pokusaji:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(22, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(249, 25);
            this.label3.TabIndex = 32;
            this.label3.Text = "Broj preostalih pokusaja:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 227);
            this.Controls.Add(this.button_C);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_A);
            this.Controls.Add(this.label_MissedLtrCnt);
            this.Controls.Add(this.button_B);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_D);
            this.Controls.Add(this.button_H);
            this.Controls.Add(this.label_Word);
            this.Controls.Add(this.button_G);
            this.Controls.Add(this.button_LoadNewWord);
            this.Controls.Add(this.button_Z);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_F);
            this.Controls.Add(this.label_MissedLetters);
            this.Controls.Add(this.button_P);
            this.Controls.Add(this.button_E);
            this.Controls.Add(this.button_M);
            this.Controls.Add(this.button_U);
            this.Controls.Add(this.button_I);
            this.Controls.Add(this.button_N);
            this.Controls.Add(this.button_V);
            this.Controls.Add(this.button_R);
            this.Controls.Add(this.button_J);
            this.Controls.Add(this.button_O);
            this.Controls.Add(this.button_T);
            this.Controls.Add(this.button_L);
            this.Controls.Add(this.button_S);
            this.Controls.Add(this.button_K);
            this.Name = "Form1";
            this.Text = "Vjesala";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_A;
        private System.Windows.Forms.Button button_B;
        private System.Windows.Forms.Button button_C;
        private System.Windows.Forms.Button button_D;
        private System.Windows.Forms.Button button_E;
        private System.Windows.Forms.Button button_F;
        private System.Windows.Forms.Button button_G;
        private System.Windows.Forms.Button button_H;
        private System.Windows.Forms.Button button_M;
        private System.Windows.Forms.Button button_N;
        private System.Windows.Forms.Button button_O;
        private System.Windows.Forms.Button button_P;
        private System.Windows.Forms.Button button_K;
        private System.Windows.Forms.Button button_L;
        private System.Windows.Forms.Button button_J;
        private System.Windows.Forms.Button button_I;
        private System.Windows.Forms.Button button_U;
        private System.Windows.Forms.Button button_V;
        private System.Windows.Forms.Button button_S;
        private System.Windows.Forms.Button button_T;
        private System.Windows.Forms.Button button_R;
        private System.Windows.Forms.Button button_Z;
        private System.Windows.Forms.Label label_MissedLetters;
        private System.Windows.Forms.Button button_LoadNewWord;
        private System.Windows.Forms.Label label_MissedLtrCnt;
        private System.Windows.Forms.Label label_Word;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
    }
}

